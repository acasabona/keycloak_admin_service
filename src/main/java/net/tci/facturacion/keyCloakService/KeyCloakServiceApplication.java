package net.tci.facturacion.keyCloakService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeyCloakServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeyCloakServiceApplication.class, args);
	}
}
