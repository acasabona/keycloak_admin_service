package net.tci.facturacion.keyCloakService.controller;

import net.tci.facturacion.keyCloakService.controller.request.CambiarContraseniaRequest;
import net.tci.facturacion.keyCloakService.controller.request.UserRequest;
import net.tci.facturacion.keyCloakService.controller.response.AccessTokenResponse;
import net.tci.facturacion.keyCloakService.controller.response.Respuesta;
import org.keycloak.OAuth2Constants;
import org.keycloak.RSATokenVerifier;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.IDToken;
import org.keycloak.representations.RefreshToken;
import org.keycloak.representations.idm.*;
import org.keycloak.util.TokenUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.*;

import static java.util.Arrays.asList;

@RequestMapping("/user")
@RestController
public class UserController {

    @Value("${app-properties.keycloak.server.auth}")
    private String urlServerAuth;
    @Value("${app-properties.keycloak.realm}")
    private String realmName;
    @Value("${app-properties.keycloak.clientID}")
    private String clienteID;

    @PostMapping("/new")
    @CrossOrigin
    public Respuesta registerUser(@RequestBody UserRequest request) {

        Respuesta response = new Respuesta();
        try {
            request.getKeycloakAccess().setClientId(clienteID);
            Keycloak kc = Keycloak.getInstance(urlServerAuth,
                    realmName,
                    request.getKeycloakAccess().getClientId(),
                    request.getKeycloakAccess().getTokenAccess());

            /** CREACION DE CREDENCIAL PARA EL NUEVO USUARIO */
            CredentialRepresentation credential = new CredentialRepresentation();
            credential.setType(CredentialRepresentation.PASSWORD);
            credential.setValue(request.getPassword());
            credential.setTemporary(false);
            /*************************************************/

            /** DEFINIENDO INFORMACION DEL NUEVOUSUARIO*/
            UserRepresentation user = new UserRepresentation();
            user.setUsername(request.getUsername());
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setCredentials(asList(credential));
            user.setEnabled(true);

            Map<String, List<String>> map = new HashMap<>();
            map.put("idPse", asList(request.getIdPse()));
            user.setAttributes(map);
            // Create testuser
            Response result = kc.realm(realmName).users().create(user);

            response.setMensaje(result.getStatusInfo().getReasonPhrase());

            /** Obteniendo ID de usuario creado */
            String userId = result.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
            result.close();


            // Obteniendo usuario creado por el id
           /* int numero = 0;
            for (int i = 0 ; kc.realm(realmName).users().list().size()> i; i++){
                if (kc.realm(realmName).users().list().get(i).getId().equals(userId)) {
                    numero = i;
                    break;
                }
            }*/
            // Guardando en grupo de su ruc
            /*ArrayList<String> algo = new ArrayList<>();
            algo.add("diego3");
            UsersResource usersResource = kc.realm(realmName).users();
            UserRepresentation users = usersResource.search(request.getUsername()).get(0);
            users.setEmail("lupin@hogwarts.co.uk");
            usersResource.get(users.getId()).update(user);*/
            // UserRepresentation usuario = kc.realm(realmName).users().list().get(numero);
            //     usuario.setGroups(Collections.singletonList("12345678912"));


            /*UsersResource usersResource = kc.realm(realmName).users();
            UserRepresentation users = usersResource.search("lupin").get(0);
            users.setEmail("lupin@hogwarts.co.uk");
            usersResource.get(user.getId()).update(user);*/


            RealmResource realmResource = kc.realm(realmName);
            UsersResource userResource = realmResource.users();

            ClientRepresentation client = realmResource.clients()
                    .findByClientId(request.getKeycloakAccess().getClientId()).get(0);

            //client.get

            System.out.println("Cliente obtenido:" + client.getClientId());

            /**Se asigna los roles que administra el cliente "test-client" */
            List<RoleRepresentation> listaRolesCliente = realmResource.clients().get(client.getId()).roles().list();
            if (!ObjectUtils.isEmpty(listaRolesCliente) && !ObjectUtils.isEmpty(request.getRolesList())) {
                asignarRoles(userResource, userId, client, realmResource, listaRolesCliente, request.getRolesList());
            }

            kc.close();
            response.setEstado(true);
            response.setMensaje("Operacion correcta");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    @PostMapping("/cambiarPassword")
    @CrossOrigin
    public Respuesta cambiarPassword(@RequestBody CambiarContraseniaRequest request) {
        Respuesta response = new Respuesta();
       try{
        request.getKeycloakAccess().setClientId(clienteID);
        Keycloak kc = Keycloak.getInstance(urlServerAuth,
                realmName,
                request.getKeycloakAccess().getClientId(),
                request.getKeycloakAccess().getTokenAccess());

        /** CREACION DE CREDENCIAL PARA EL NUEVO USUARIO */
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(request.getPassword());
        credential.setTemporary(false);

        UsersResource usersResource = kc.realm(realmName).users();
        UserRepresentation users = usersResource.search(request.getUsername()).get(0);
        users.setCredentials(asList(credential));
       // users.setEmail("algo@hotmail.com");
        usersResource.get(users.getId()).update(users);

        kc.close();
        response.setEstado(true);
        response.setMensaje("se cambio la contraseña");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }


    private void asignarRoles(UsersResource userResource, String userId, ClientRepresentation client,
                              RealmResource realmResource, List<RoleRepresentation> listaRolesCliente,
                              List<String> rolesList) {
        List<RoleRepresentation> lista = new ArrayList<>();
        for (String rolName : rolesList) {
            for (RoleRepresentation rol : listaRolesCliente) {
                if (rol.getName().equals(rolName)) {
                    lista.add(rol);
                    break;
                }
            }
        }
        userResource.get(userId).roles().clientLevel(client.getId()).add(lista);
    }

    @PostMapping("/refrescarToken")
    @CrossOrigin
    public Respuesta refrescarToken(@RequestBody CambiarContraseniaRequest request) {

        //Refresh Token
        String responses = OAuth2Constants.REFRESH_TOKEN;// doClientCredentialsGrantAccessTokenRequest("secret1");
        //responses.getRefreshToken();

        //RefreshToken token = TokenUtil.getRefreshToken(request.getKeycloakAccess().getTokenAccess());
    /*  oauth.doLogin("test-user@localhost", "password");

        EventRepresentation loginEvent = events.expectLogin().assertEvent();

        String sessionId = loginEvent.getSessionId();
        String codeId = loginEvent.getDetails().get(Details.CODE_ID);

        String code = oauth.getCurrentQuery().get(OAuth2Constants.CODE);

        OAuthClient.AccessTokenResponse tokenResponse = oauth.doAccessTokenRequest(code, "password");
        AccessToken token = oauth.verifyToken(tokenResponse.getAccessToken());
        String refreshTokenString = tokenResponse.getRefreshToken();
        RefreshToken refreshToken = oauth.verifyRefreshToken(refreshTokenString);

        EventRepresentation tokenEvent = events.expectCodeToToken(codeId, sessionId).assertEvent();

        Assert.assertNotNull(refreshTokenString);

        assertEquals("bearer", tokenResponse.getTokenType());

        Assert.assertThat(token.getExpiration() - getCurrentTime(), allOf(greaterThanOrEqualTo(200), lessThanOrEqualTo(350)));
        int actual = refreshToken.getExpiration() - getCurrentTime();
        Assert.assertThat(actual, allOf(greaterThanOrEqualTo(1799), lessThanOrEqualTo(1800)));

        assertEquals(sessionId, refreshToken.getSessionState());

        setTimeOffset(2);

        OAuthClient.AccessTokenResponse response = oauth.doRefreshTokenRequest(refreshTokenString, "password");
        AccessToken refreshedToken = oauth.verifyToken(response.getAccessToken());
        RefreshToken refreshedRefreshToken = oauth.verifyRefreshToken(response.getRefreshToken());

        assertEquals(200, response.getStatusCode());

        assertEquals(sessionId, refreshedToken.getSessionState());
        assertEquals(sessionId, refreshedRefreshToken.getSessionState());

        Assert.assertThat(response.getExpiresIn(), allOf(greaterThanOrEqualTo(250), lessThanOrEqualTo(300)));
        Assert.assertThat(refreshedToken.getExpiration() - getCurrentTime(), allOf(greaterThanOrEqualTo(250), lessThanOrEqualTo(300)));

        Assert.assertThat(refreshedToken.getExpiration() - token.getExpiration(), allOf(greaterThanOrEqualTo(1), lessThanOrEqualTo(10)));
        Assert.assertThat(refreshedRefreshToken.getExpiration() - refreshToken.getExpiration(), allOf(greaterThanOrEqualTo(1), lessThanOrEqualTo(10)));

        Assert.assertNotEquals(token.getId(), refreshedToken.getId());
        Assert.assertNotEquals(refreshToken.getId(), refreshedRefreshToken.getId());

        assertEquals("bearer", response.getTokenType());

        assertEquals(findUserByUsername(adminClient.realm("test"), "test-user@localhost").getId(), refreshedToken.getSubject());
        Assert.assertNotEquals("test-user@localhost", refreshedToken.getSubject());

        assertEquals(1, refreshedToken.getRealmAccess().getRoles().size());
        Assert.assertTrue(refreshedToken.getRealmAccess().isUserInRole("user"));

        assertEquals(1, refreshedToken.getResourceAccess(oauth.getClientId()).getRoles().size());
        Assert.assertTrue(refreshedToken.getResourceAccess(oauth.getClientId()).isUserInRole("customer-user"));

        EventRepresentation refreshEvent = events.expectRefresh(tokenEvent.getDetails().get(Details.REFRESH_TOKEN_ID), sessionId).assertEvent();
        Assert.assertNotEquals(tokenEvent.getDetails().get(Details.TOKEN_ID), refreshEvent.getDetails().get(Details.TOKEN_ID));
        Assert.assertNotEquals(tokenEvent.getDetails().get(Details.REFRESH_TOKEN_ID), refreshEvent.getDetails().get(Details.UPDATED_REFRESH_TOKEN_ID));

        setTimeOffset(0);*/


        return null;
    }

}
