package net.tci.facturacion.keyCloakService.controller.request;

public class KeycloakAccess {

    private String clientId;
    private String tokenAccess;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getTokenAccess() {
        return tokenAccess;
    }

    public void setTokenAccess(String tokenAccess) {
        this.tokenAccess = tokenAccess;
    }
}
