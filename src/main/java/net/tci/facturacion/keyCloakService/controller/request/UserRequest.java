package net.tci.facturacion.keyCloakService.controller.request;

import java.util.List;

public class UserRequest {

    private KeycloakAccess keycloakAccess;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String idPse;
    private List<String> rolesList;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdPse() {
        return idPse;
    }

    public void setIdPse(String idPse) {
        this.idPse = idPse;
    }


    public KeycloakAccess getKeycloakAccess() {
        return keycloakAccess;
    }

    public void setKeycloakAccess(KeycloakAccess keycloakAccess) {
        this.keycloakAccess = keycloakAccess;
    }

    public List<String> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<String> rolesList) {
        this.rolesList = rolesList;
    }
}
