package net.tci.facturacion.keyCloakService.controller;

import net.tci.facturacion.keyCloakService.controller.request.SubGroupRequest;
import net.tci.facturacion.keyCloakService.controller.response.SubGrupoResponse;
import net.tci.facturacion.keyCloakService.controller.response.Respuesta;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/subGrupo")
@RestController
public class SubGrupoController {
    @Value("${app-properties.keycloak.server.auth}")
    private String urlServerAuth;
    @Value("${app-properties.keycloak.realm}")
    private String realmName;
    @Value("${app-properties.keycloak.clientID}")
    private String clienteID;

    private static void createSubGroups(Keycloak keycloak, String realmName, String idGroupName, String nameSubGroup) {
        GroupRepresentation parentSub = null;
        boolean found = false;
        for (GroupRepresentation group : keycloak.realm(realmName).groups().groups()) {
            //for (GroupRepresentation sub: group.getSubGroups()){
            // if (sub.getName().equals(groupName))
            if (group.getId().equals(idGroupName)) {
                parentSub = group;
                found = true;
                break;
            }
            //}
            if (found){ break;}
        }
        if (found) {
            GroupResource parentSubResource = keycloak.realm(realmName).groups().group(parentSub.getId());
            GroupRepresentation subGroup1 = new GroupRepresentation();
            subGroup1.setName(nameSubGroup);
            //subGroup1.setPath("/Group_1/" + groupName);
            subGroup1.setPath("/" + nameSubGroup);
            parentSubResource.subGroup(subGroup1);
        }

    }

    @PostMapping("/new")
    @CrossOrigin
    public SubGrupoResponse registerGroup(@RequestBody SubGroupRequest request) {
        SubGrupoResponse response = new SubGrupoResponse();
        Respuesta mensaje = new Respuesta();
        try {
            Keycloak kc = Keycloak.getInstance(urlServerAuth,
                    realmName,
                    clienteID,
                    request.getKeycloakAccess().getTokenAccess());
            createSubGroups(kc, realmName,request.getIdGrupo(), request.getNameSubGroup());
            kc.close();
            mensaje.setEstado(true);
            mensaje.setMensaje("Operacion correcta");
            response.setUserResponse(mensaje);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
