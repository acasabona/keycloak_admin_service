package net.tci.facturacion.keyCloakService.controller;

import net.tci.facturacion.keyCloakService.controller.request.AccessTokenRequest;
import net.tci.facturacion.keyCloakService.controller.response.AccessTokenResponse;
import org.keycloak.admin.client.Keycloak;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

@RestController
public class AdminController {

    @Context
    SecurityContext securityContext;
    @Value("${app-properties.keycloak.server.auth}")
    private String urlServerAuth;
    @Value("${app-properties.keycloak.realm}")
    private String realmName;
    @Value("${app-properties.keycloak.clientID}")
    private String clienteID;

    @PostMapping(value = "/authenticate")
    @CrossOrigin
    public AccessTokenResponse getAccessToken(@RequestBody AccessTokenRequest request) {
        AccessTokenResponse response = new AccessTokenResponse();
        try {
            Keycloak keycloak = Keycloak.getInstance(
                    urlServerAuth,
                    realmName,
                    request.getUsuario(),
                    request.getContrasenia(),
                    clienteID);

            response.setAccessToken(keycloak.tokenManager().getAccessToken().getToken());
            response.setRefreshToken(keycloak.tokenManager().getAccessToken().getRefreshToken());
            response.setExpiry(keycloak.tokenManager().getAccessToken().getExpiresIn());
            response.setRefreshEspiry(keycloak.tokenManager().getAccessToken().getRefreshExpiresIn());
            keycloak.close();

            /*public static Keycloak getInstance(String serverUrl, String realm, String username, String password, String clientId) {
                return new Keycloak(serverUrl, realm, username, password, clientId, (String)null, "password", (ResteasyClient)null, (String)null);
            }*/
            /*public static Keycloak getInstance(String serverUrl, String realm, String clientId, String authToken) {
                return new Keycloak(serverUrl, realm, (String)null, (String)null, clientId, (String)null, "password", (ResteasyClient)null, authToken);
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}


