package net.tci.facturacion.keyCloakService.controller.response;

public class Respuesta {

    private boolean estado;
    private String mensaje;


    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
