package net.tci.facturacion.keyCloakService.controller;

import net.tci.facturacion.keyCloakService.controller.request.*;
import net.tci.facturacion.keyCloakService.controller.response.AccessTokenResponse;
import net.tci.facturacion.keyCloakService.controller.response.Respuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RequestMapping("/simulacion")
@RestController
public class ProcesoUsuarioController {
    @Autowired
    GroupController groupController;
    @Autowired
    UserController userController;
    @Autowired
    AdminController adminController;

    @PostMapping("/creacionCliente")
    @CrossOrigin
    public Respuesta registerUser(@RequestBody ProcesoUsuarioRequest request) {

        AccessTokenRequest accessTokenRequest = new AccessTokenRequest();
        accessTokenRequest.setUsuario("dsantos");
        accessTokenRequest.setContrasenia("12345");
        AccessTokenResponse token = adminController.getAccessToken(accessTokenRequest);

        KeycloakAccess keycloakAccess = new KeycloakAccess();
        keycloakAccess.setClientId("veloseService");
        keycloakAccess.setTokenAccess(token.getAccessToken());

        GroupRequest grupo = new GroupRequest();
        grupo.setKeycloakAccess(keycloakAccess);
        grupo.setNameGroup(request.getRuc());
        groupController.registerGroup(grupo);

        ArrayList<String> algo = new ArrayList();
        algo.add("modificar-grupos");
        algo.add("view-groups");

        UserRequest user = new UserRequest();
        user.setKeycloakAccess(keycloakAccess);
        user.setUsername(request.getUsuario());
        user.setPassword("123456");
        user.setRolesList(algo);
        userController.registerUser(user);
        //Asignar el usuario a Grupo Creado




        return null;
    }

}
