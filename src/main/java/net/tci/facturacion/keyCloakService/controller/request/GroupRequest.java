package net.tci.facturacion.keyCloakService.controller.request;

public class GroupRequest {

    private KeycloakAccess keycloakAccess;
    private String nameGroup;

    public KeycloakAccess getKeycloakAccess() {
        return keycloakAccess;
    }

    public void setKeycloakAccess(KeycloakAccess keycloakAccess) {
        this.keycloakAccess = keycloakAccess;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }
}
