package net.tci.facturacion.keyCloakService.controller.response;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;

public class GrupoResponse {
    private String id;
    private String nombre;
    private List<GroupRepresentation> responseService;
    private List<GrupoResponse> subGrupos;

    List<String> grupo;
    Respuesta response;

    public List<String> getGrupo() {
        return grupo;
    }

    public void setGrupo(List<String> grupo) {
        this.grupo = grupo;
    }

    public Respuesta getResponse() {
        return response;
    }

    public void setResponse(Respuesta response) {
        this.response = response;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<GrupoResponse> getSubGrupos() {
        return subGrupos;
    }

    public void setSubGrupos(List<GrupoResponse> subGrupos) {
        this.subGrupos = subGrupos;
    }

    public List<GroupRepresentation> getResponseService() {
        return responseService;
    }

    public void setResponseService(List<GroupRepresentation> responseService) {
        this.responseService = responseService;
    }
}
