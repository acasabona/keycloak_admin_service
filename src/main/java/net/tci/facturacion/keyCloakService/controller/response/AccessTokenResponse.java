package net.tci.facturacion.keyCloakService.controller.response;

import java.io.Serializable;

public class AccessTokenResponse implements Serializable {

    private String accessToken;
    private String refreshToken;
    private long expiry;
    private long refreshEspiry;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpiry() {
        return expiry;
    }

    public void setExpiry(long expiry) {
        this.expiry = expiry;
    }

    public long getRefreshEspiry() {
        return refreshEspiry;
    }

    public void setRefreshEspiry(long refreshEspiry) {
        this.refreshEspiry = refreshEspiry;
    }
}
