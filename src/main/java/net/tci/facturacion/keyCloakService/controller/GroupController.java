package net.tci.facturacion.keyCloakService.controller;

import net.tci.facturacion.keyCloakService.controller.request.GroupRequest;
import net.tci.facturacion.keyCloakService.controller.response.GrupoResponse;
import net.tci.facturacion.keyCloakService.controller.response.Respuesta;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/group")
@RestController
public class GroupController {
    @Value("${app-properties.keycloak.server.auth}")
    private String urlServerAuth;
    @Value("${app-properties.keycloak.realm}")
    private String realmName;

    @PostMapping("/new")
    @CrossOrigin
    public Respuesta registerGroup(@RequestBody GroupRequest request) {
        Respuesta response = new Respuesta();
        try {
            request.getKeycloakAccess().setClientId("veloseService");
            Keycloak kc = Keycloak.getInstance(urlServerAuth,
                    realmName,
                    request.getKeycloakAccess().getClientId(),
                    request.getKeycloakAccess().getTokenAccess());
            //Metodo Para creacion de Grupo
            GroupRepresentation groupRepresentation = new GroupRepresentation();
            groupRepresentation.setName(request.getNameGroup());
            Response result = kc.realm(realmName).groups().add(groupRepresentation);
            result.close();
           // kc.tokenManager().refreshToken();
            kc.close();
            response.setEstado(true);
            response.setMensaje("Operacion correcta");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @PostMapping("/lista")
    @CrossOrigin
    public GrupoResponse listarGrupos(@RequestBody GroupRequest request) {
        GrupoResponse response = new GrupoResponse();
        try {
            request.getKeycloakAccess().setClientId("veloseService");
            Keycloak kc = Keycloak.getInstance(urlServerAuth,
                    realmName,
                    request.getKeycloakAccess().getClientId(),
                    request.getKeycloakAccess().getTokenAccess());
            List<GroupRepresentation> result = kc.realm(realmName).groups().groups();
            //List<GrupoResponse> transfor = transfor(result);
            //response.setSubGrupos(transfor);
            response.setResponseService(result);
            kc.close();

            response.getResponse().setEstado(true);
            response.getResponse().setMensaje("Operacion correcta");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public List<GrupoResponse> transfor(List<GroupRepresentation> result) {
        List<GrupoResponse> lstResult = new ArrayList<>();
        for (GroupRepresentation gr : result) {
            GrupoResponse response = new GrupoResponse();
            response.setId(gr.getId());
            response.setNombre(gr.getName());

            if (gr.getSubGroups() != null && !gr.getSubGroups().isEmpty()) {
                response.setSubGrupos(transfor(gr.getSubGroups()));
            }
            lstResult.add(response);
        }
        return lstResult;
    }

}
