//package net.tci.facturacion.keyCloakService.controller;
//
//import org.keycloak.OAuthErrorException;
//import org.keycloak.jose.jws.JWSInput;
//import org.keycloak.jose.jws.JWSInputException;
//import org.keycloak.models.RealmModel;
//import org.keycloak.representations.AccessToken;
//import org.keycloak.representations.RefreshToken;
//
//public class RefrescarToken extends AccessToken {
//
//
//    public RefreshToken verifyRefreshToken(RealmModel realm, String encodedRefreshToken) throws OAuthErrorException, JWSInputException {
//        JWSInput jws = new JWSInput(encodedRefreshToken);
//        RefreshToken refreshToken = null;
//        try {
//           // if (!RSAProvider.verify(jws, realm.getPublicKey())) {
//           // if (!RSAProvider.verify(jws, realm.getId())) {
//           //     throw new OAuthErrorException(OAuthErrorException.INVALID_GRANT, "Invalid refresh token");
//           // }
//            refreshToken = jws.readJsonContent(RefreshToken.class);
//        } catch (Exception e) {
//            throw new OAuthErrorException(OAuthErrorException.INVALID_GRANT, "Invalid refresh token", e);
//        }
//        if (refreshToken.getExpiration() != 0 && refreshToken.isExpired()) {
//            throw new OAuthErrorException(OAuthErrorException.INVALID_GRANT, "Refresh token expired");
//        }
//
//        if (refreshToken.getIssuedAt() < realm.getNotBefore()) {
//            throw new OAuthErrorException(OAuthErrorException.INVALID_GRANT, "Stale refresh token");
//        }
//        return refreshToken;
//    }
//
//    /*
//    private AccessTokenResponse getToken() throws ClientProtocolException, IOException {
//        HttpClient client = new HttpClientBuilder().disableTrustManager().build();
//        try {
//            HttpPost post = new HttpPost(KeycloakUriBuilder.fromUri("http://localhost:8080/auth")
//                    .path(ServiceUrlConstants.TOKEN_SERVICE_DIRECT_GRANT_PATH).build("rest-demo"));
//            List<NameValuePair> formparams = new ArrayList<NameValuePair>();
//            formparams.add(new BasicNameValuePair(OAuth2Constants.GRANT_TYPE, "password"));
//            formparams.add(new BasicNameValuePair("username", "user"));
//            formparams.add(new BasicNameValuePair("password", "pass"));
//
//            //will obtain a token on behalf of angular-product-app
//            formparams.add(new BasicNameValuePair(OAuth2Constants.CLIENT_ID, "angular-product-app"));
//
//
//            UrlEncodedFormEntity form = new UrlEncodedFormEntity(formparams, "UTF-8");
//            post.setEntity(form);
//            HttpResponse response = client.execute(post);
//            int status = response.getStatusLine().getStatusCode();
//            HttpEntity entity = response.getEntity();
//            if (status != 200) {
//                throw new IOException("Bad status: " + status);
//            }
//            if (entity == null) {
//                throw new IOException("No Entity");
//            }
//            InputStream is = entity.getContent();
//            try {
//                AccessTokenResponse tokenResponse = JsonSerialization.readValue(is, AccessTokenResponse.class);
//                return tokenResponse;
//            } finally {
//                try {
//                    is.close();
//                } catch (IOException ignored) {
//                }
//            }
//        } finally {
//            client.getConnectionManager().shutdown();
//        }
//    }*/
//}
