package net.tci.facturacion.keyCloakService.controller.request;

public class SubGroupRequest {

    private KeycloakAccess keycloakAccess;
    private String nameSubGroup;
    private String idGrupo;

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public KeycloakAccess getKeycloakAccess() {
        return keycloakAccess;
    }

    public void setKeycloakAccess(KeycloakAccess keycloakAccess) {
        this.keycloakAccess = keycloakAccess;
    }

    public String getNameSubGroup() {
        return nameSubGroup;
    }

    public void setNameSubGroup(String nameSubGroup) {
        this.nameSubGroup = nameSubGroup;
    }
}
