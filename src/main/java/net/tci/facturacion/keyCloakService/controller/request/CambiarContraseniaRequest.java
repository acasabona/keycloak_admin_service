package net.tci.facturacion.keyCloakService.controller.request;

public class CambiarContraseniaRequest {

    private String username;
    private String password;
    private KeycloakAccess keycloakAccess;

    //private String tokenAccess;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public String getTokenAccess() {
//        return tokenAccess;
//    }
//
//    public void setTokenAccess(String tokenAccess) {
//        this.tokenAccess = tokenAccess;
//    }


    public KeycloakAccess getKeycloakAccess() {
        return keycloakAccess;
    }

    public void setKeycloakAccess(KeycloakAccess keycloakAccess) {
        this.keycloakAccess = keycloakAccess;
    }
}
